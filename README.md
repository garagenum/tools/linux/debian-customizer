# Réaliser la post-install sur une debian existante.

1. Téléchargez le projet ``wget https://gitlab.com/garagenum/linux-admin-tools/debian-customizer/-/archive/master/debian-customize-master.tar``
2. Extraire le contenu de l'archive : ``tar -xvf debian-customizer-master.tar``
3. Renommer le dossier : ``mv debian-customizer-master debian-customizer``
4. Entrer dans le répertoire d'éxecution ``cd debian-customizer/``
3. Exécutez le script custom-debian.sh avec ``sudo sh custom-debian.sh``
4. Vous accédez alors à une boîte de dialogue pour choisir les options


# Installer un serveur PXE sur une machine

1. Clônez le projet ``git clone wget https://gitlab.com/garagenum/linux-admin-tools/debian-customizer.git``
2. Entrez dans le répertoire d'éxecution ``cd debian-customizer/``
3. Exécutez le script custom-debian.sh avec ``sudo sh install-pxe-server.sh``
4. Vous aurez besoin d'être connectés à Internet

# Utiliser le serveur PXE pour installer une Debian

1. Reliez le client et le serveur avec un câble ethernet
2. Démarrez le client en PXE
3. Suivez les indications pour l'installation
4. Après redemérrage, connectez-vous à Internet éxécutez les scripts verif-packages.sh (en sudo) et creer_admin.sh qui sont dans $HOME/bellinuxien
5. Vous pouvez accéder à tous les fichiers de customisation dans /opt/custom-debian-conf/

# Modifier l'installateur

1. Modifier ./pxe-conf/tftp/preseed/custom-install-preseed.sh
2. Créer un script de post-install personnalisé: "custom-debian-conf/scripts/config-xyz.txt"
3. Modifier ./custom-debian-conf/scripts/garage-custom.sh 


# Télécharger le netboot avec les firmware nonfree  

1. La doc sur https://wiki.debian.org/fr/DebianInstaller/NetbootFirmware  
2. Les images Netboot sur https://www.debian.org/distrib/netinst#netboot