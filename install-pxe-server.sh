#!/bin/bash

# For logs and debug purpose

exec 5> >(logger -st $0)
BASH_XTRACEFD="5"
PS4='$LINENO: '
set -x

if (whiptail  --title "Écraser les modifications" --defaultno --yesno "Faut-il écraser les modifications locales et mettre à jour le projet Debian-customizer sur ce serveur Pxe ?" 15 80) then
	git reset --hard
	git clean -fd
	git pull
fi

#Running path : custom-debian-conf
tail -n +$[LINENO+5] $0 |exec sudo bash 
NORMAL_USER=bellinuxien
chown -Rf $NORMAL_USER:$NORMAL_USER /home/$NORMAL_USER

exit 

exec 5> >(logger -st $0)
BASH_XTRACEFD="5"
PS4='$LINENO: '
set -x
select_iface(){
	#sudo su
	TITLE="Interface réseau reliée au Web"
	select_iface_ask
	IFIN=$IFACE
	IPIN=$IFACEIP
	TITLE="Interface réseau reliée aux ordinateurs à installer"
	select_iface_ask
	IFOUT=$IFACE
	IPOUT=$IFACEIP
	if (whiptail --title "Vous avez choisi :" --yesno "Interface réseau WAN reliée au web: $IFIN $IPIN\nInterface réseau LAN reliée aux clients: $IFOUT $IPOUT\nEst-ce correct ?" 15 80) then
		config
	else
		$0 #retour au début du script
	fi
}

select_iface_ask(){
	DISPLAY=()
	INTERFACES=$(ip l | grep -E '[a-z].*: ' | sed 's/ //' |cut -d ':' -f2) #On isole les interfaces dans une variable $INTERFACES
	set $INTERFACES #Chaque interface isolée par la variable $INTERFACES est inscrite dans un argument

	for i in $@ #On cherche la variable $i représentant une interface dans la liste des arguments
	do
		IP=$(ip a | grep -E "$i$" | cut -d ' ' -f6) #On isole l'IP de l'interface sur laquelle on boucle
		DISPLAY+=("$i" "$IP") #Dans ce cas, on affiche l'interface et son IP mises en page pour Whiptail, avec un \ à la fin de la ligne
	done

	IFACE=$(whiptail --title "$TITLE" --menu "Choisir :" 15 60 4 \
		"${DISPLAY[@]}" 3>&1 1>&2 2>&3)

	exitstatus=$?
	if [ $exitstatus = 0 ]; then
		whiptail --title "$TITLE" --msgbox "Vous avez choisi l'interface : $IFACE." 10 60
		IFACEIP=$(ip a | grep $IFACE | grep inet | sed -e "s/  //gw /dev/stderr" | cut -d ' ' -f2)
	else
		whiptail --title "$TITLE" --msgbox "Vous avez annulé" 10 60
                select_iface
	fi
}

main(){
	SELECT=$(whiptail --clear --separate-output --title "Que voulez-vous exécuter?" --checklist \
	"Choisissez parmi les services suivants:" 15 90 10 \
	select_iface "Reconfgurer les interfaces réseau du serveur PXE" OFF \
	mirror "Mettre à jour le miroir Debian" ON \
	packages "Mettre à jour les logiciels serveur PXE" ON \
	tftp "Mettre à jour le noyau Linux de l'installateur" OFF \
	live "Mettre à jour le live" OFF \
	send_tar "Mettre à jour la configuration des OS à installer" OFF  3>&1 1>&2 2>&3)
	## La redirection  3>&1 1>&2 2>&3 permet de récupérer la sortie de whiptail dans $SELECT
	for i in $SELECT; do
		case $i in 
			"select_iface")
				select_iface ;;
			"packages")
				packages ;;
			"tftp")
				tftp ;;
			"live")
				live ;;
			"send_tar")
				send_tar ;;
			"mirror")
				mirror ;;
		esac
	done
}

send_tar() {
	#cd custom-debian-conf
	tar -cf custom-debian-conf.tar custom-debian-conf
	mv -f custom-debian-conf.tar /srv/tftp/
	#cd ..
}

packages(){
	apt update
	apt upgrade
	DEBIAN_FRONTEND=noninteractive apt-get -o Dpkg::Options::="--force-confnew" -y --assume-yes --allow-unauthenticated -f install nfs-kernel-server nfs-common isc-dhcp-server tftpd-hpa iptables-persistent apache2 apt-mirror curl jq git wget isolinux squashfs-tools screen links2 libpam-cgfs xorriso tmux
}

mirror(){ 
	cd pxe-conf/server-conf
	mkdir -p /var/mirror
	cp -f mirror.list /var/mirror/
	apt-mirror /var/mirror/mirror.list
	sh /var/mirror/var/clean.sh  
	ln -s /var/mirror /var/www/html/mirror
	ln -s /var/mirror/mirror/deb.debian.org/debian-security/dists/bullseye /var/mirror/mirror/deb.debian.org/debian-security/dists/stable
	ln -s /var/mirror/mirror/deb.debian.org/debian/dists/bullseye /var/mirror/mirror/deb.debian.org/debian/dists/stable
	ln -s /var/mirror/mirror/deb.debian.org/debian/dists/bullseye-backports /var/mirror/mirror/deb.debian.org/debian/dists/stable-backports
	ln -s /var/mirror/mirror/deb.debian.org/debian/dists/bullseye-updates /var/mirror/mirror/deb.debian.org/debian/dists/stable-updates
	cd ../../
}


config(){
    cd pxe-conf/server-conf
	# Configuration d'iptables pour la redirection de l'interface $IFIN vers $IFOUT
	echo 1 > /proc/sys/net/ipv4/ip_forward 
	iptables -t nat -F
	iptables -t nat -A POSTROUTING -s 192.168.30.0/24 -o $IFIN -j MASQUERADE 
	iptables-save > /etc/iptables/rules.v4
	#
	cp -f NetworkManager.conf /etc/NetworkManager/
	cp -f interfaces /etc/network/ 
	sed -i "s/ifout/$IFOUT/g" /etc/network/interfaces
	cp -f sysctl.conf /etc/ 
	cp -f dhcpd.conf /etc/dhcp/ 
	cp -f isc-dhcp-server /etc/default/
	sed -i "s/INTERFACES.*/INTERFACESv4=$IFOUT/g" /etc/default/isc-dhcp-server 
	cp -f tftpd-hpa /etc/default/
	cp -f exports /etc/
#	mkdir -p /home/bellinuxien/bin/
#	cp -pf updatepxe /home/bellinuxien/bin/
#	chown -Rfv bellinuxien:bellinuxien /home/bellinuxien/bin
#	echo "export PATH='/home/bellinuxien/bin:$PATH'" >> /home/bellinuxien/.bashrc
	cd ../../
}

live(){
        rm -Rfv /srv/tftp/debian-live/debian-live-amd64
        mkdir -p /srv/tftp/debian-live/debian-live-amd64/
	rm  debian-cd/current-live/amd64/iso-hybrid/debian*.iso
        wget -c -r -np -nH -A "debian-live-*-amd64-mate.iso" https://cdimage.debian.org/debian-cd/current-live/amd64/iso-hybrid/
        mkdir -p isoloop
        mount -o loop debian-cd/current-live/amd64/iso-hybrid/debian*.iso isoloop
        cp -rf isoloop/live/* /srv/tftp/debian-live/debian-live-amd64/
        mv /srv/tftp/debian-live/debian-live-amd64/vmlinuz* /srv/tftp/debian-live/debian-live-amd64/vmlinuz
        mv /srv/tftp/debian-live/debian-live-amd64/initrd.img* /srv/tftp/debian-live/debian-live-amd64/initrd.img
	#
	# Customize Debian live
	#
	## Creating chroot env
	#
	mkdir -p live-workdir
	cd live-workdir
	unsquashfs ../isoloop/live/filesystem.squashfs
	mount --bind /dev squashfs-root/dev
        mount --bind /sys squashfs-root/sys
        mount --bind /proc squashfs-root/proc
	#
	## Copy custom definition
	#
	cp -Rf ../custom-debian-conf squashfs-root/opt/
	#
	## Chroot
	#
	chroot squashfs-root /bin/bash << "EOT"
	export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
	#
	### Locales
	# 
	rm -vf /etc/console-setup/cached*
	echo "Europe/Paris" > /etc/timezone
	sed -i -e 's/# fr_FR.UTF-8/fr_FR.UTF-8/' /etc/locale.gen
	locale-gen
	cp -vf /opt/custom-debian-conf/system-conf/keyboard-live /etc/default/keyboard
	#
	### Apt
	#
	echo "nameserver 8.8.8.8" > /etc/resolv.conf
	cp /opt/custom-debian-conf/system-conf/sources.list /etc/apt/
	apt update
	DEBIAN_FRONTEND=noninteractive apt -y --allow-unauthenticated install wget
	cp /opt/custom-debian-conf/system-conf/google-chrome.list /etc/apt/sources.list.d/
	wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
        apt update
	export DEBIAN_FRONTEND=noninteractive
	cat /opt/custom-debian-conf/paquets/common/*.list |xargs apt -y --allow-unauthenticated install
	apt remove -y snapd
        apt install -f
	apt clean
	#
	### Gsettings
	#
	mkdir -p /etc/dconf/profile
	mkdir -p /etc/dconf/db/local.d/
	cp -v /opt/custom-debian-conf/system-conf/dconf-profile /etc/dconf/profile/user
 	cp -v /opt/custom-debian-conf/system-conf/mate-live-dconf /etc/dconf/db/local.d/00-live-mate-desktop
	dconf update
	# 
	### Add user
	useradd -m -s /bin/bash -p '$6$27/NXPO4M$JnFiJigr/LBcduzEusDpEYCUBc2mepcqW9lmejwBA28MZSgsB2koxmFHP0fZ6sOGtfzXbSyi7Sfko.ebbeyQh.' bellinuxien
	usermod -a -G sudo bellinuxien
	#
	## Exit chroot
	#
EOT
	umount squashfs-root/dev
	umount squashfs-root/proc
	umount squashfs-root/sys
	#
	## Create filesystem.squashfs
	#
	mksquashfs squashfs-root filesystem.squashfs -comp xz
	mv -vf filesystem.squashfs /srv/tftp/debian-live/debian-live-amd64/filesystem.squashfs
	#
	## Create iso
	#
	mkdir custom-live
        cp -Rf ../isoloop/* custom-live/
	cp -Rf ../isoloop/.disk custom-live/
        cd custom-live
	echo "Debian 11 Live Mate Custom" > .disk/info
	rm -R live/*
	cp -fv /srv/tftp/debian-live/debian-live-amd64/* live/
	sed -i 's/-[[:digit:]]*.[[:digit:]]*.[[:digit:]]*-[[:digit:]]*-amd64//g' isolinux/menu.cfg
	xorriso -as mkisofs -V 'Debian 11 Live Mate Custom' -o ../../debian-live-11-custom-amd64-nonfree.iso -J -J -joliet-long -cache-inodes -isohybrid-mbr /usr/lib/ISOLINUX/isohdpfx.bin -b isolinux/isolinux.bin -c isolinux/boot.cat -boot-load-size 4 -boot-info-table -no-emul-boot -eltorito-alt-boot -e boot/grub/efi.img -no-emul-boot -isohybrid-gpt-basdat -isohybrid-apm-hfsplus .
	#
	# Clean
	#
	cd ../../
        umount isoloop
        rm -Rf isoloop
	umount live-workdir/squashfs-root/*
	rm -Rf live-workdir
	rm -Rf debian-cd
}

tftp(){
	cd pxe-conf/tftp/
	
	rm -Rf /srv/tftp/{debian-installer,efi64,efi32,legacybios}
	
	wget -O netboot-amd64.tar.gz http://ftp.nl.debian.org/debian/dists/bullseye/main/installer-amd64/current/images/netboot/netboot.tar.gz
	tar -xf netboot-amd64.tar.gz
	rm  netboot-amd64.tar.gz
	cd debian-installer/amd64
	
	cp -p initrd.gz initrd.gz.orig
	wget -O firmware-amd64.cpio.gz http://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/firmware.cpio.gz
	cat initrd.gz.orig firmware-amd64.cpio.gz > initrd.gz
	rm -f initrd.gz.orig firmware-amd64.cpio.gz
	cd ../../

	wget -O netboot-i386.tar.gz http://ftp.nl.debian.org/debian/dists/bullseye/main/installer-i386/current/images/netboot/netboot.tar.gz
	tar -xf netboot-i386.tar.gz
	rm  netboot-i386.tar.gz
	cd debian-installer/i386
	cp -p initrd.gz initrd.gz.orig
	wget -O firmware-i386.cpio.gz http://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/firmware.cpio.gz
	cat initrd.gz.orig firmware-i386.cpio.gz > initrd.gz
	rm -f initrd.gz.orig firmware-i386.cpio.gz
	cd ../../

	cp -rf debian-installer efi* legacybios preseed /srv/tftp/
	cd ../../
}

main
