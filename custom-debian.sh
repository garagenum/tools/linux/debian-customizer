#!/bin/bash

exec 5> >(logger -st $0) || true
BASH_XTRACEFD="5"
PS4='$LINENO: '
set -x

#Custom already installed Debian

ANSWER="/opt/custom-debian-conf/answer.value"

git reset --hard
git pull

cp_to_opt(){
	sudo rm -Rf /opt/custom-debian-conf
	sudo cp -Rf custom-debian-conf /opt/
	cd /opt/custom-debian-conf
}

if [[ ! -f $ANSWER ]] 
then
	echo "answer.value not in /opt"
	cp_to_opt
	bash scripts/garage-custom.sh
else
	echo "already existing answer.value in /opt : "
	cat $ANSWER
	CHOIX=$(cat $ANSWER |sed s/\"//g |sed s/,/\ /g) 
	if (whiptail --title "existing profiles"  --yesno \
	"Faut-il conserver uniquement les profils existants: '\n' $CHOIX " 15 90); then
		echo "keep old profiles selected"
		cp -vf $ANSWER /tmp/
		cp_to_opt
		cp -vf /tmp/answer.value .
		tail -n +32 scripts/garage-custom.sh |bash
	else
		echo "new profile to select"
		cp_to_opt
		pwd
		bash scripts/garage-custom.sh
	fi
fi
