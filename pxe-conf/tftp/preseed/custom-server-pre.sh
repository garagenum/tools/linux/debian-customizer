#!/bin/sh

#############################################################
###########                                              ####
##########  Téléchargement et décompression des fichiers ####
##########                                               ####
#############################################################

tftp -g -r custom-debian-conf.tar 192.168.30.1
tar -xvf custom-debian-conf.tar

cd custom-debian-conf

echo "d-i pkgsel/include string $(for i in $(ls paquets/common/*.list | grep -v wan);\
    do cat $i ; done | sed 's/$/ \\/' | sed '/^\ /d' | sed '$s/\\//' )" > server-packages.cfg

debconf-set-selections server-packages.cfg
