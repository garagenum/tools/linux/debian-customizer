#!/bin/sh

set -x

# This is a debconf-compatible script
. /usr/share/debconf/confmodule

#############################################################
###########                                              ####
##########  Téléchargement et décompression des fichiers ####
##########                                               ####
#############################################################

tftp -g -r custom-debian-conf.tar 192.168.30.1
tar -xvf custom-debian-conf.tar

# Declare variable for log file path
cd custom-debian-conf
VERIF=post-log.txt
ANSWER=answer.value

chmod u+x scripts/*


#############################################################
###########                                              ####
##########  1ère question                                ####
##########                                               ####
############################################################# 

# Create the template file
cat > /tmp/myquestion.template << '!EOF!'
Template: my-question/ask
Type: multiselect
Choices: Enfants, Seniors, Gamer, MAO, Team-Working, Toshiba_toughbook
Default: Enfants, Seniors
Description: Profils à sélectionner

Template: my-question/title
Type: text
Description: Qui utilise l'ordinateur ?

!EOF!

# Load your template
debconf-loadtemplate my-question /tmp/myquestion.template

# Set title for your custom dialog box
db_settitle my-question/title

# Ask it!
db_input critical my-question/ask
db_go

# Get the answer
db_get my-question/ask

# Save it to a file
echo "selected profiles : $RET" > $VERIF
echo "$RET" >  $ANSWER
echo "cat answer.value : $(cat $ANSWER)"

#############################################################
###########                                              ####
##########  2eme question                                ####
##########                                               ####
############################################################# 

cat > /tmp/nextquestion.template << '!EOF2!'
Template: next-question/ask
Type: multiselect
Choices: mate-desktop, gnome-desktop, kde-desktop, xfce-desktop, lxde-desktop, cinnamon-desktop
Choices-fr.UTF-8: Mate, Gnome, Kde, Xfce, Lxde, Cinnamon
Default: gnome-desktop
Description: Environnements de Bureau

Template: next-question/title
Type: text
Description: Quel environnement de bureau souhaitez-vous utiliser? 

!EOF2!

# Load your template
debconf-loadtemplate next-question /tmp/nextquestion.template

# Set title for your custom dialog box
db_settitle next-question/title

# Ask it!
db_input critical next-question/ask
db_go

# Get the answer
db_get next-question/ask

# Save it to a file
echo "desktop env chosen is : $RET" | tee -a $VERIF
echo $RET >> $ANSWER

echo $RET > desktop-answer.value
DESKTOP=$(cat desktop-answer.value)

dpkg --add-architecture i386


CHOIX=$(cat $ANSWER | sed s/,/\ /g)

FIRST_MULTISELECT="tasksel tasksel/first multiselect standard, laptop, print-server"

if [ -n "$DESKTOP" ]; then
	echo "$FIRST_MULTISELECT, desktop, $DESKTOP" > extra-packages.cfg
else
	echo "$FIRST_MULTISELECT" > extra-packages.cfg
fi

#echo "d-i pkgsel/include string  $(for i in $(cat custom-debian-conf/answer.value | sed s/,/\ /g); \
#    do for l in $(ls custom-debian-conf/paquets/$i/*.list | grep -v wan); \
#    do cat $l ; done ; done | sed 's/$/ \\/' | sed '/^\ /d'; \
#    for l in $(ls custom-debian-conf/paquets/common/*.list | grep -v wan); \
#    do cat $l ;done | sed 's/$/ \\/' | sed '/^\ /d' | sed '$s/.$//')" \
#     >> extra-packages.cfg

echo "d-i pkgsel/include string  $(for i in $CHOIX; do \
	for l in $(ls paquets/$i/*.list | grep -v wan); do \
	cat $l ; done ; done | sed 's/$/ \\/' | sed '/^\ /d'; \
    for l in $(ls paquets/common/*.list | grep -v wan); do \
    cat $l ;done | sed 's/$/ \\/' | sed '/^\ /d' | sed '$s/.$//')" \
     >> extra-packages.cfg

debconf-set-selections extra-packages.cfg

for i in $CHOIX ; do 
case $i in
  "gnome-desktop")
    echo "gnome choisi" > $VERIF 
    cat scripts/config-gnome.sh >> scripts/post.sh 
    ;;
  "Enfants")
    echo "enfants choisis" >> $VERIF 
    ;;
  "Gamer")
    echo "gamer choisi" >> $VERIF
    cat scripts/config-gamer.sh >> scripts/post.sh 
    ;;
  "Seniors")
    echo "vieux choix" >> $VERIF 
    ;;
  "Toshiba_toughbook")
    echo "Toshiba Toughbook" >> $VERIF
    cat scripts/config-toshiba_toughbook.txt >> scripts/post.sh 
    ;;
  "Team-Working")
    echo "team-working choisi" >> $verif ;
    cat scripts/config-team-working.sh >> scripts/post.sh
    ;;
  *) echo "none of gnome, enfants, gamer, seniors, team-working nor Toshiba chosen." >> $VERIF 
  ;;
esac ;
done
 
