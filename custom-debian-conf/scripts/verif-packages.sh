#!/bin/bash

#exec 5> >(logger -st $0)
#BASH_XTRACEFD="5"
#PS4='$LINENO: '
set -x

PAQ=paquets
COM=common
echo "we are in verif-packages.sh, inside $(pwd)"

cd /opt/custom-debian-conf

export verif=post-log.txt
export CHOIX=$(cat answer.value | sed s/,/\ /g | sed s/\"/\ /g )

bash scripts/treat-packages.sh



#cp -rT /etc/skel /home/bellinuxien
#chown -Rf bellinuxien:bellinuxien /home/bellinuxien

#systemctl disable verif-packages.service
#reboot
