#!/bin/bash

#exec 5> >(logger -st $0)
BASH_XTRACEFD="5"
PS4='$LINENO: '
set -x

export verif=post-log.txt
export CHOIX=$(cat answer.value | sed s/,/\ /g | sed s/\"/\ /g )
#cp verif-packages.sh /etc/init.d/
#ln -s verif-packages.sh /etc/init.d/

#Redirection des messages sur syslog
#exec 1> >(logger -s -t $(basename $0)) 2>&1

#Des configs DEBCONF pour éviter un bug à l'install de ttf-mscorefonts-installer (https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=439763):
export DEBIAN_FRONTEND=noninteractive
unset  DEBIAN_HAS_FRONTEND
unset  DEBCONF_REDIR
unset  DEBCONF_OLD_FD_BASE

#sed -i s/.destkop/.desktop/ /etc/gnome/defaults.list # correction d'un bu$
#Remplacer totem par VLC, puisque totem plante...
#sed -i.bak s/totem.desktop/vlc.desktop/ /etc/gnome/defaults.list

# Configuration
##################


system_configuration(){
	# Declaration for complete path of conf files being modified 
	# Use double quotes if pathscontains special caracters
	declare -a CONF_FILES
	CONF_FILES+=(/var/lib/AccountsService/users/bellinuxien)  # bellinuxien not in user list at login screen
	CONF_FILES+=(/etc/lightdm/lightdm.conf)                    # congif login screen
	CONF_FILES+=(/etc/adduser.conf)                            # modify default adduser (add extra groups)
	CONF_FILES+=(/usr/share/applications/mimeapps.list)        # associate extensions with applications (e.g. .odt)
	CONF_FILES+=(/etc/apt/sources.list)  				        # list of .deb respositories
	CONF_FILES+=(/etc/apt/sources.list.d/google-chrome.list)	# .deb repo. for Google Chrome
	CONF_FILES+=(/etc/logrotate.conf)							# settings for logs keeping limit
	CONF_FILES+=(/etc/logrotate.d/rsyslog)				# that on too
	#CONF_FILES+=(/etc/ntp.conf)								# configure a diffent time server
	CONF_FILES+=(/etc/cron-apt/config)                             # config automatic updates
	CONF_FILES+=(/etc/cron-apt/action.d/3-download)				# Configure aptitude command in cron
	CONF_FILES+=(/var/lib/gdm3/.config/pulse/default.pa)				# Avoid GDM blocking bluetooth
	CONF_FILES+=(/home/bellinuxien/.config/autostart/icones_addusers.desktop)  # add scripts on bellinuxien desktop
	CONF_FILES+=(/home/bellinuxien/.config/autostart/conky.desktop)   # conky launcher
	CONF_FILES+=(/home/bellinuxien/.conkyboot.sh)						# conky script
	CONF_FILES+=(/home/bellinuxien/.conkyrc)							# conky conf file
	CONF_FILES+=(/etc/systemd/system/addvisiteur.service)				# service to add user visiteur
	CONF_FILES+=(/etc/systemd/system/verif-packages.service)				# service to verif packages at 1st startup

	for i in "${CONF_FILES[@]}" ; do           # [@] means 'all elements of array
	  echo "treating  ${i##*/} ..."            # get only filename (delete everything before last / )
	  mkdir -vp "${i%/*}"                      # get path without filename (see doc: Transformation of Variables)
	  [[ -f "$i.orig" ]] || (cp -f "$i" "$i.orig" && echo "    backup done as $i.orig")
	  cp -f "system-conf/${i##*/}" "$i" &&	echo "    SUCCESS: $i modified"    ;   
	done

	#Configuration des lanceurs sur le bureau
	mkdir -p /etc/skel/Bureau
	(cp -vf lanceurs/*.desktop /etc/skel/Bureau && echo ".desktop files copied on desktop") |tee $verif

	#Ajout d'un profil Firefox avec Ublock
	bash scripts/firefox-profile-install.sh

	chown -Rfv bellinuxien:bellinuxien /home/bellinuxien

	systemctl daemon-reload
	systemctl enable verif-packages.service
	systemctl enable addvisiteur.service
}

system_configuration
bash scripts/treat-packages.sh
