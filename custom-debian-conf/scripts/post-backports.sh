#!/bin/bash

verif=post-log.txt
CHOIX=$(cat answer.value | sed s/,/\ /g)
PAQ=paquets
COM=common
#cp verif-packages.sh /etc/init.d/
#ln -s verif-packages.sh /etc/init.d/

#Redirection des messages sur syslog
#exec 1> >(logger -s -t $(basename $0)) 2>&1

#Des configs DEBCONF pour éviter un bug à l'install de ttf-mscorefonts-installer (https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=439763):
export DEBIAN_FRONTEND=noninteractive
unset  DEBIAN_HAS_FRONTEND
unset  DEBCONF_REDIR
unset  DEBCONF_OLD_FD_BASE

#sed -i s/.destkop/.desktop/ /etc/gnome/defaults.list # correction d'un bu$
#Remplacer totem par VLC, puisque totem plante...
#sed -i.bak s/totem.desktop/vlc.desktop/ /etc/gnome/defaults.list

# Configuration
##################

# Copie des fichiers de configuration de l'écran de connexion

	# Copie du fichier contenant la liste des utilisateurs n'apparaissant pas sur l'écran de connexion

if ! /etc/lightdm/users.conf.orig
then 
	cp -vf /etc/lightdm/users.conf /etc/lightdm/users.conf.orig;
fi
cp -vf system-conf/users.conf /etc/lightdm/users.conf && echo "j'ai transfere le users.conf" >>$verif

	#Copie du fichier de configuration de l'écran de connexion

if ! /etc/lightdm/lightdm.conf.orig
then 
	cp -vf /etc/lightdm/lightdm.conf /etc/lightdm/lightdm.conf.orig;
fi
cp -vf system-conf/lightdm.conf /etc/lightdm/lightdm.conf && echo "j'ai transfere le lightdm.conf" >>$verif

# Copie du fichier de configuration pour la création d'utilisateurs
if ! /etc/adduser.conf.orig
then
        cp -vf /etc/adduser.conf /etc/adduser.conf.orig;
fi
cp -vf system-conf/adduser.conf /etc/adduser.conf && echo "j'ai transfere le adduser.conf" >>$verif

# Copie du fichier d'associations extensions - applications

mkdir -p /usr/share/applications

if ! /usr/share/applications/mimeapps.list.orig
then
        cp -vf /usr/share/applications/mimeapps.list /usr/share/applications/mimeapps.list.orig;
fi
cp -vf system-conf/mimeapps.list /usr/share/applications && echo "j'ai transfere le mimeapps.list" >> $verif


#Modification du Skel

#Modification des signets

if ! /etc/skel/.gtk-bookmarks.orig
then 
	cp -vf /etc/skel/.gtk-bookmarks /etc/skel/.gtk-bookmarks.orig;
fi
cp -vf system-conf/.gtk-bookmarks /etc/skel/.gtk-bookmarks&& echo "j'ai transfere le .gtk-bookmarks" >>$verif

#Ajout de scripts au démarrage des sessions

mkdir -p /etc/skel/.config/autostart
cp -v system-conf/update_icones.desktop /etc/skel/.config/autostart && echo "j'ai transfere les scripts de demarrage de session" >> verif;

#Configuration des lanceurs sur le bureau

mkdir -p /etc/skel/Bureau #rajout de l'option -p pour éviter les erreurs
cp -v lanceurs/*.desktop /etc/skel/Bureau && echo "j'ai transfere les lanceurs" >>$verif 

# Copie du sources.list (liste des dépots)

if ! /etc/apt/sources.list.orig
then 
	cp -vf /etc/apt/sources.list /etc/apt/sources.list.orig && echo "sources.list sauvegardé" >> $verif ;
fi
cp -vf system-conf/sources.list.wan.debian /etc/apt/sources.list && echo "sources.list modifié" >> $verif

apt-get update

# Installation des paquets à partir du réseau externe (internet)

apt-get -y --allow-unauthenticated install \
$(for l in $(ls $PAQ/$COM/wan.list); do
cat $l
done) && echo "common packages installed from net in backports" >> $verif

apt-get -y --allow-unauthenticated install \
$(for i in $CHOIX; do
for l in $(ls $PAQ/$i/wan.list); do
cat $l
done
done) && echo "selected packages installed from net in backports" >> $verif

apt-get clean && echo "fin des actions sur les paquets" >> $verif

# Copie du fichier preferences pour apt

cp -vf system-conf/backports /etc/apt/preferences.d && echo "fichier preferences apt modifié" >> $verif
apt-get update
apt-get -y --allow-unauthenticated upgrade
apt-get -y --allow-unauthenticated dist-upgrade && echo "mise à jour vers backports faite" >> $verif

# Réglages pour les mises à jour automatiques
# On a installé cron-apt
# On configure d'abord le sources.list utilisé par cron-apt
if ! /etc/cron-apt/config.orig
then
	cp -vf /etc/cron-apt/config /etc/cron-apt/config.orig;
fi
mkdir -p /etc/cron-apt
cp -vf system-conf/config /etc/cron-apt/config

# On configure ensuite l'usage de la commande aptitude
if ! /etc/cron-apt/action.d/3-download.orig
then
	cp -vf /etc/cron-apt/action.d/3-download /etc/cron-apt/action.d/3-download.orig;
fi
mkdir -p /etc/cron-apt/action.d
cp -vf system-conf/3-download /etc/cron-apt/action.d/3-download


#On ajoute un script pour ajouter des éléments sur le bureau des utilisateurs
mkdir -p /home/bellinuxien/.config/autostart
cp -vf system-conf/icones_addusers.desktop /home/bellinuxien/.config/autostart/
chown -Rfv bellinuxien:bellinuxien /home/bellinuxien/.config

#On ajoute un service au démarrage, pour la création du deuxième utilisateur , et pour vérifier l'installation des paquets.

cp -vf system-conf/addvisiteur.service /etc/systemd/system/addvisiteur.service
cp -vf system-conf/verif-packages.service /etc/systemd/system/verif-packages.service
systemctl --system reload
systemctl enable verif-packages.service
systemctl enable addvisiteur.service
