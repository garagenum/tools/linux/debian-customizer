#!/bin/bash
        wget -O- https://dl.google.com/linux/linux_signing_key.pub |gpg --dearmor > /etc/apt/trusted.gpg.d/google.gpg
        apt update

        # Installation des paquets à partir du réseau externe (internet)
        ( cat paquets/common/wan.list |xargs apt-get -y --allow-unauthenticated install )  && echo "common packages installed from net by post.sh" |tee $verif

	# Configuration de flatpak et installation des flatpak
	flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
	find paquets/common -name *.flatpak -exec bash {} \; && echo "common flatpak OK" |tee $verif
	find paquets/common -name *.snap -exec bash {} \; && echo "common snap OK" |tee  $verif

        for i in $CHOIX; do
                ( cat "paquets/$i/wan.list" |xargs apt-get -y --allow-unauthenticated install ) && echo "selected packages installed from net by post.sh" |tee $verif ;
		find paquets/$i/*.flatpak -exec bash {} \; && echo "selected flatpak OK" |tee $verif
		find paquets/$i/*.snap -exec bash {} \; && echo "selected snap OK" |tee $verif
        done

        #Configuration de libdvd-pkg pour installer libdvdcss
        dpkg-reconfigure libdvd-pkg

        # Install Veyon and VisualCodeStudio for Users
        wget https://github.com/veyon/veyon/releases/download/v4.5.7/veyon_4.5.7-0-debian-bullseye_amd64.deb
        mv veyon_4.5.7-0-debian-bullseye_amd64.deb /var/cache/apt/archives/
        apt install -y --allow-unauthenticated /var/cache/apt/archives/veyon_4.5.7-0-debian-bullseye_amd64.deb

        # Install minetest lug9000, a subgame by Le Garage Numérique
        MT_GAMES_DIR=/var/lib/flatpak/app/net.minetest.Minetest/current/active/files/share/minetest/games && mkdir -p $MT_GAMES_DIR \
         && LUG_VERSION=$(curl -s  https://gitlab.com/api/v4/projects/9700916/releases |jq -r '.[0].name') \
         &&  curl $(curl -s  https://gitlab.com/api/v4/projects/9700916/releases |jq -r '.[0].assets.sources[0].url') -o $MT_GAMES_DIR/lug9000.zip \
         && unzip -qfo $MT_GAMES_DIR/lug9000.zip -d $MT_GAMES_DIR && rm $MT_GAMES_DIR/lug9000.zip \
         && mv -vf $MT_GAMES_DIR/lug9000-$LUG_VERSION $MT_GAMES_DIR/lug9000 \
         && cat << EOF >> $MT_GAMES_DIR/../minetest.conf
keymap_forward = KEY_KEY_Z
keymap_left = KEY_KEY_Q
keymap_zoom = KEY_KEY_W
keymap_drop = KEY_KEY_A
EOF
        #mkdir -p /var/lib/snapd/seed/
        #cp -vf system-conf/gnome-seed.yaml /var/lib/snapd/seed/seed.yaml 
        apt clean && echo "fin des actions sur les paquets by post.sh" |tee $verif

