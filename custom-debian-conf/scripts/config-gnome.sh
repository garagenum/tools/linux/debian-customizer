#!/bin/bash
WORKING_DIR=$(pwd)
mkdir -p /etc/skel/.icons 
cd system-conf
# icon theme
wget $(curl -s https://github.com/keeferrourke/la-capitaine-icon-theme/releases/latest |cut -d \" -f 2 |sed s/"releases\/tag"/archive/ |sed s/$/.zip/) -O la-capitaine-icon-theme.zip
unzip la-capitaine-icon-theme.zip
mv la-capitaine-icon-theme-* la-capitaine-icon-theme
rm la-capitaine-icon-theme.zip
cd .. 
cp -Rf system-conf/la-capitaine-icon-theme* /etc/skel/.icons/ && echo "la-capitaine-icon-theme copied to skel"
# wallpaper
cp -Rf system-conf/Images /etc/skel/
# extensions
mkdir -p /etc/skel/.local/share/gnome-shell/extensions
cd /etc/skel/.local/share/gnome-shell/extensions

declare -A EXTENSIONS
EXTENSIONS=(
  [ding]="https://extensions.gnome.org/extension-data/dingrastersoft.com.v33.shell-extension.zip"
  [dash-to-panel]="https://extensions.gnome.org/extension-data/dash-to-paneljderose9.github.com.v42.shell-extension.zip"
  [arcmenu]="https://extensions.gnome.org/extension-data/arcmenuarcmenu.com.v17.shell-extension.zip"
  [user-theme]="https://extensions.gnome.org/extension-data/user-themegnome-shell-extensions.gcampax.github.com.v42.shell-extension.zip"
  [BingWallpaper]="https://extensions.gnome.org/extension-data/BingWallpaperineffable-gmail.com.v36.shell-extension.zip"  
)

for i in "${!EXTENSIONS[@]}" ; do
  wget "${EXTENSIONS[$i]}" && 
  unzip -o "$(sed -e "s;https://extensions.gnome.org/extension-data/;;" -e 's/\%40/@/' <<<${EXTENSIONS[$i]})" \
    -d $(sed -e "s;https://extensions.gnome.org/extension-data/$i;$i@;" -e 's/\%40//' \
      -e 's/.v[[:digit:]]*.shell-extension.zip//' <<<${EXTENSIONS[$i]})  &&
  echo "extraction succeeded for $i, in folder : "
  echo "    $(sed -e "s;https://extensions.gnome.org/extension-data/$i;$i@;" -e 's/\%40//' \
      -e 's/.v[[:digit:]]*.shell-extension.zip//' <<<${EXTENSIONS[$i]})"
done


cd $WORKING_DIR

rm -v /home/bellinuxien/.config/dconf/user
mkdir -p /etc/dconf/profile
mkdir -p /etc/dconf/db/local.d/
cp -v system-conf/dconf-profile /etc/dconf/profile/user
cp -v system-conf/gnome-profile.dump /etc/dconf/db/local.d/00-gn-desktop
dconf update
cat << EOF > /etc/skel/.config/autostart/gio.desktop
[Desktop Entry]
Name=Change l'attribut "trusted" pour l'extension gnome DING
Exec=/opt/custom-debian-conf/scripts/gio.sh
Type=Application
StartupNotify=true
EOF
