#!/bin/sh

#exec 5> >(logger -st $0) ||true
BASH_XTRACEFD="5"
PS4='$LINENO: '
set -x

################################################
######					########
######		1ère question		#######
######					#######
###############################################

#Réalisé à partir de http://xmodulo.com/create-dialog-boxes-interactive-shell-script.html

whiptail --clear --title "Profils" --checklist \
"Quels profils d'utilisateurs sur cet ordinateur?" 15 90 10 \
mate-desktop "pour les utilisateurs de l'environnement de bureau Mate" OFF \
gnome-desktop "environnement de bureau moderne" OFF \
Enfants "Des jeux et des outils éducatifs" ON \
Seniors "Des outils d'accessibilité" ON \
Gamer "Des jeux, des jeux ,des jeux" OFF \
MAO "Des logiciels pour la création musicale" OFF \
Team-Working "Des logiciels pour le travail collaboratif" OFF \
Toshiba_toughbook "Correction de bug pour le son" OFF  2>answer.value

echo "answer.value is "
cat answer.value


# After here we can start using this script in case answer.value had \
# already been installed (see custom-debian.sh l.36)

dpkg --add-architecture i386


CHOIX=$(cat answer.value |sed s/\"//g |sed s/,/\ /g)
echo $CHOIX

verif=post-log.txt

apt update

for i in $CHOIX ; do
	case $i in
		*-desktop)
			tasksel install $i ;;
	esac
done



PACKAGES=$(for i in $CHOIX; \
    do for l in $(ls paquets/$i/*.list |grep -v wan); \
    do cat $l ; done ; done; \
    for l in $(ls paquets/common/*.list |grep -v wan); \
    do cat $l ;done)
apt-get -y --allow-unauthenticated --show-progress install $PACKAGES
echo $PACKAGES > extra-packages.cfg

for i in $CHOIX ; do 
case $i in
  "gnome-desktop")
    echo "gnome choisi" > $verif ;
    cat scripts/config-gnome.sh >> scripts/post.sh ;;
  "Enfants")
   echo "enfants choisis" >> $verif ;;
  "Gamer")
    echo "gamer choisi" >> $verif ;
    cat scripts/config-gamer.sh >> scripts/post.sh ;;
  "Seniors")
    echo "vieux choix" >> $verif ;;
#    cat scripts/config-seniors.txt >> scripts/post.sh ;;
  "Team-Working")
     echo "team-working choisi" >> $verif ;
     cat scripts/config-team-working.sh >> scripts/post.sh ;;
  "Toshiba_toughbook")
  echo "Toshiba Toughbook" >> $verif ;
  cat scripts/config-toshiba_toughbook.txt >> scripts/post.sh ;;
  *) echo "none of gnome, enfants, gamer, seniors, Toshiba, chosen." >> $verif ;;
esac
done

bash scripts/post.sh
