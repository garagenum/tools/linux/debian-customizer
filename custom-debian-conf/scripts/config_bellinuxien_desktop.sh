# /bin/bash

cp -v /opt/custom-debian-conf/system-conf/creer_admin.sh /home/bellinuxien/
cp -v /opt/custom-debian-conf/system-conf/creer_utilisateur.sh /home/bellinuxien/
cp -v /opt/custom-debian-conf/scripts/verif-packages.sh /home/bellinuxien/
cp -v /opt/custom-debian-conf/system-conf/.conkyrc /home/bellinuxien/.conkyrc
chown -Rfv bellinuxien:bellinuxien /home/bellinuxien/Bureau/*.sh
chmod u+x /home/bellinuxien/Bureau/*.sh
