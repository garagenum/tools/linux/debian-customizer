#!/bin/bash

echo "Enter in git-lfs from : "
pwd
cd ..
git clone https://gitlab.com/garagenum/linux-admin-tools/debian-customizer.git
cd debian-customizer/custom-debian-conf
git-lfs pull || true
mkdir -p /opt/games
mkdir -p /var/lib/flatpak/exports/share/applications
cp -vf games/*.iso /opt/games/
cp -vf games/desktopfiles/* /var/lib/flatpak/exports/share/applications/
cd ../../custom-debian-conf
rm -Rf ../debian-customizer
