
# Correction d'un bug de cartes intel (driver i915)
i915_kernel_options='i915.semaphores=1 i915.i915_enable_rc6=0'
if grep i915 /proc/bus/pci/devices; then
  sed -i .bak \
    -e "s/GRUB_CMDLINE_LINUX=\"/GRUB_CMDLINE_LINUX=\"$i915_kernel_options/" \
    /etc/default/grub
fi
